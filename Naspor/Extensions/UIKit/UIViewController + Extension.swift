//
//  UIViewController + Extension.swift
//  Naspor
//
//  Created by Евгения Кирюшина on 29.04.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func configure<T: SelfConfiguratingCell, U: Hashable>(collectionView: UICollectionView, celltype: T.Type, with value: U, for indexPath: IndexPath) -> T {
           guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: celltype.reuseId, for: indexPath) as? T else {
               fatalError("Unable to dequeue \(celltype)") }
           cell.configure(with: value)
           return cell
       }
}
