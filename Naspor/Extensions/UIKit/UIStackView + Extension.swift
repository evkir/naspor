//
//  UIStackView + Extension.swift
//  Naspor
//
//  Created by Евгения Кирюшина on 06.04.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

extension UIStackView {
    
    convenience init(arrangedSubviews: [UIView], axis: NSLayoutConstraint.Axis, spacing: CGFloat){
        self.init(arrangedSubviews: arrangedSubviews)
        self.axis = axis
        self.spacing = spacing 
    }
}
