//
//  MChat.swift
//  Naspor
//
//  Created by Евгения Кирюшина on 29.04.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

struct MChat: Hashable, Decodable {
    var username: String
    var userImageString: String
    var lastMessage: String
    var id: Int
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: MChat, rhs: MChat) -> Bool {
        return lhs.id == rhs.id
    }
}
