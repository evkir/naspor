//
//  WaitingChatCell.swift
//  Naspor
//
//  Created by Евгения Кирюшина on 29.04.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class WaitingChatCell: UICollectionViewCell, SelfConfiguratingCell {
    
    static var reuseId: String = "WaitingChatCell"
    
    let friendImageView = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .yellow
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
        setupConstraints()
    }
    
    func configure<U>(with value: U) where U : Hashable {
        guard let chat: MChat = value as? MChat else { return }
        friendImageView.image = UIImage(named: chat.userImageString)
    }
    
    private func setupConstraints() {
        friendImageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(friendImageView)
        
        NSLayoutConstraint.activate([
            friendImageView.topAnchor.constraint(equalTo: self.topAnchor),
            friendImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            friendImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            friendImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
