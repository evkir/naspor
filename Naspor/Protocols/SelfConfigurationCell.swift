//
//  SelfConfigurationCell.swift
//  Naspor
//
//  Created by Евгения Кирюшина on 29.04.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation

protocol SelfConfiguratingCell {
    static var reuseId: String { get }
    func configure<U: Hashable>(with value: U)
}
