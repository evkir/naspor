//
//  AuthNavigatingDelegate.swift
//  Naspor
//
//  Created by Евгения Кирюшина on 02.05.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation

protocol AuthNavigatingDelegate: class {
    func toLoginVC()
    func toSignUpVC()
}
