//
//  UserError.swift
//  Naspor
//
//  Created by Евгения Кирюшина on 02.05.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation

enum UserError {
    case notFilled
    case photoNotExist
    case canNotGetUserInfo
    case canNotUnwrapToMUser
}

extension UserError: LocalizedError{
    var errorDescription: String? {
        switch self {
        case .notFilled:
            return NSLocalizedString("Fill all data", comment: "")
        case .photoNotExist:
            return NSLocalizedString("User didn't chose photo", comment: "")
        case .canNotGetUserInfo:
            return NSLocalizedString("Can't get info about current user from Firebase", comment: "")
        case .canNotUnwrapToMUser:
            return NSLocalizedString("Can't unwrap MUser from User", comment: "")
        }
    }
}
