//
//  AuthError.swift
//  Naspor
//
//  Created by Евгения Кирюшина on 02.05.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation

enum AuthError {
    case notFilled
    case invalidEmail
    case passwordNotMatched
    case unknownError
    case serverError
}

extension AuthError: LocalizedError{
    var errorDescription: String? {
        switch self {
        case .notFilled:
            return NSLocalizedString("Fill all data", comment: "")
        case .invalidEmail:
            return NSLocalizedString("Invalid email format", comment: "")
        case .passwordNotMatched:
            return NSLocalizedString("Password doesn't match", comment: "")
        case .unknownError:
            return NSLocalizedString("Unknown error", comment: "")
        case .serverError:
            return NSLocalizedString("Server error", comment: "")
        }
    }
}
